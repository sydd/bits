﻿<%@ Page Title="" Language="C#" MasterPageFile="~/elmaestro.master" AutoEventWireup="true" CodeFile="quest.aspx.cs" Inherits="quest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h2>
        Bienvenido al sistema de quest
    </h2>
    <h3>
        Inicia algunas de las quest disponibles:
    </h3>
     <p>
        <asp:ListBox ID="ListBox1" runat="server" AutoPostBack="True" Height="99px" 
            onselectedindexchanged="ListBox1_SelectedIndexChanged" Width="169px" 
            Font-Bold="True" Font-Italic="False">
            <asp:ListItem Selected="True" Value="atqI">Ataque a la red I</asp:ListItem>
            <asp:ListItem Value="atqII">Ataque a la red II</asp:ListItem>
            <asp:ListItem Value="atqIII">Ataque a la red III</asp:ListItem>
        </asp:ListBox>
    </p>
    <p>
        <asp:Label ID="lblDesc" runat="server"></asp:Label>
    </p>
    
    <h3>
        Cantidad de bytes con los cuales realizar el ataque:
    </h3>    
        <asp:TextBox ID="txtAtaque" runat="server" TextMode="Number"></asp:TextBox>
    <p>
    <asp:Button ID="btnLanzar" runat="server" Text="Lanzar ataque" />    
    </p>
</asp:Content>

