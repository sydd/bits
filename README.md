# Bitquest#

Codigo en ASP.NET (con C#) para un juego simple web.


#Funcionamiento#

Cada usuario es un sistema, el sistema dispone de un escudo que sera atacado por otros sistemas (usuarios).
La 'moneda' de cambio del sistema, seran los bytes. Se usan para atacar otros sistemas, para protejer el tuyo, o para comprar items.
Los bytes, se consiguen farmeando, simplemente iniciando una session nueva en el servidor.

#Items e inventario#

Cada sistema tambien dispondra de una lista de items para mejorar su escudo, o potenciar sus ataques.
 