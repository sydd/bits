﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class quest : System.Web.UI.Page
{
    manejo control;
    protected void Page_Load(object sender, EventArgs e)
    {
        control = (manejo)Session["control"];
    }
    protected void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            if (ListBox1.SelectedIndex == 0)
                lblDesc.Text = "<h4>En esta quest atacas un sistema de redes, que tiene una defensa de nivel I.</h4>  Bytes recomendados: <b>20</b> <br> Bytes de recompensa: <b>50</b>";

            if (ListBox1.SelectedIndex == 1)
                lblDesc.Text = "<h4>En esta quest atacas un sistema de redes, que tiene una defensa de nivel II.</h4>  Bytes recomendados: <b>100</b> <br> Bytes de recompensa: <b>500</b>";

            if (ListBox1.SelectedIndex == 2)
                lblDesc.Text = "<h4>En esta quest atacas un sistema de redes, que tiene una defensa de nivel III.</h4> Bytes recomendados: 1000 <br> Bytes de recompensa: <b>10000</b>";
        }
        else
            lblDesc.Text = "Seleciona una quest.";
    }
}