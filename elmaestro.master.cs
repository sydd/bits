﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//En esta clase manejamos la mayoria de las cosas, la web maestr nunca se cierra.
public partial class elmaestro : System.Web.UI.MasterPage
{
  
    manejo control;
    int bits;

    protected void Page_Load(object sender, EventArgs e)
    {
      /* Si la pagina es un post back, te deja reinicia los bits. Por ahora lo mantendremos desactivado.
      /*  if (!Page.IsPostBack)
        {
            Session["bit"] = 0;          
        }*/

       bits = (int)Session["bit"];
       control = (manejo)Session["control"];
    }
     protected void Timer2_Tick(object sender, EventArgs e)
    {
        bits++;
        Label1.Text = bits.ToString();
        Session["bit"] =  bits;
    }
     protected void btnGuardar_Click(object sender, EventArgs e)
     {
         lblRespuesta.Text = control.guardar(bits, DateTime.Now.Millisecond);
     }
    
}
