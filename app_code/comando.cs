﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for comando
/// </summary>
public class comando
{
    string origen, parametro;
    string[] propiedad;

   
	public comando(string comand)
	{
        parametro = null;
        origen = null;
        propiedad = null;
        configurar(comand);
	}

    void configurar(string cmd)
    {
        string [] aux = cmd.Split(' ');

       
        int i = 0;
        if (aux.Length > 1)
        {
            for (int index = 0; index < aux.Length; index++)
            {
                if (aux[index].StartsWith("--"))
                {
                    propiedad[i] = aux[index];
                    i++;
                    continue;
                }
                if (index == 0)
                    origen = aux[index];
                else
                    parametro = aux[index];
            }
        }
        else
        {
            origen = cmd;
        }
    }


    public string Parametro
    {
        get { return parametro; }
        set { parametro = value; }
    }

    public string Origen
    {
        get { return origen; }
        set { origen = value; }
    }

    public string[] Propiedad
    {
        get { return propiedad; }
        set { propiedad = value; }
    }
    
}