﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Items
/// </summary>
public abstract class Programas
{
    string origen, help;


    public Programas()
    {
        origen = "ERROR";
        help = "ERROR";
    }

	public Programas(string ORigen,string HELP)
	{
        origen = ORigen;
        help = HELP;
	}

    public virtual string ejecutar(comando cmd);



    public string Origen
    {
        get { return origen; }
        set { origen = value; }
    }



    public string Help
    {
        get { return help; }
        set { help = value; }
    }
    
}